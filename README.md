palette
=======

Installer votre distrib xBuntu grâce à un Buntufile !

Le formalisme du Buntufile est inspiré du Berksfile :  http://berkshelf.com/ et
du Gemfile http://bundler.io/ mais sans les virgules :

    source 'clef' 'dépôt'
    deb curl
    deb docker.io 0.8

## Installation
Avant d'installer **palette** vous devez installer **ShellFactory** :
https://github.com/cchaudier/shellfactory

Pour installer **palette**:

    git clone git://github.com/artisanslogiciel/palette.git 

Puis modifier votre profile (~/.bash_profile, ~/.bashrc, ~/.zshrc etc.) avec la ligne suivante :

    export PATH=$PATH:$HOME/palette
    
    
<div>Icons made by <a href="https://www.flaticon.com/authors/good-ware" title="Good Ware">Good Ware</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
